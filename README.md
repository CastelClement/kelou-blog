![built_status](https://gitlab.com/CastelClement/kelou-blog/badges/master/pipeline.svg?style=flat-square)
![coverage](https://gitlab.com/CastelClement/kelou-blog/badges/master/coverage.svg?style=flat-square)


# kelou.fr

## What is kelou.fr

/

  

## Instructions

### Start a server (using Docker)

`sudo docker-compose up`

  

### Run commands using Docker in containers

```shell
sudo docker-compose exec [[container_name]] [[command]]
```

Example : `sudo docker-compose exec php bin/console d:m:m`

### Deploy to prod
1. Local: `./create-archive.sh`
2. Upload file to server (in /public_html/)
3. Remote: `cd public_html && tar zxf FILENAME.tar.gz && rm -rf vendor && rm -rf node_modules && composer install --optimize-autoloader && npm install && npm audit fix && npm run build && php bin/console doctrine:migrations:migrate && php bin/console cache:clear && rm FILENAME.tar.gz`

### Install in local
1. Clone the project in local using Gitkraken (& Gitlab)
2. Create docker-compose structure : `sudo docker-compose up`
3. Install composer dependencies : `sudo docker-compose exec php composer install`
4. Install npm dependencies : `npm install && npm fix`
5. Build style/scripts with Webpack : `npm run build`
6. Export DB content+Medias from production, then import in local
7. Check that everything is working (http://127.0.0.1:8000/)


### How to export database content from production
1. Access Adminer on the prod server (https://kelou.fr/adminer-4.8.1.php)
2. Connect (dbname/username/password can be found in .env file)
3. Select "Export" in the top left corner
4. Choose these parameters :

| Sortie          | gzip                                         |
|-----------------|----------------------------------------------|
| Format          | SQL                                          |
| Base de données | ✓ Routines                                   |
| Tables          | DROP+CREATE \| ☓ Incrément \| ✓ Déclencheurs |
| Données         | TRUNCATE+INSERT                              |

### How to import database content in local
1. Access Adminer on the local server (http://127.0.0.1:8888/)
2. Connect (dbname/username/password can be found in .env file)
3. Select "Import" in the top left corner
4. Choose file, then import


### How to export media content from production
1. Access "Garfield" > File Manager
2. Compress folders
```
public_html
└── public
    ├── media
    │   └── cache
    │       └── post_thumbnail
    │           └── medias
    └── medias
```
3. Extract content in local at the same location

## App links
### Web Server : http://localhost:8000/
### Adminer : http://localhost:8888/