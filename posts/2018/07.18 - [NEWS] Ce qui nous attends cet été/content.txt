L’été est là mais pas de vacances pour le blog car j’ai prévu un programme qui devrait vous plaire !

Tout d’abord, je me suis pris deux semaines de vacances les deux dernières semaines. La première car il y avait les résultats du bac (dont nous parlerons plus tard) et ensuite car il y avait la finale de Coupe du Monde (ON EST LES CHAMPIONS !!). Mais maintenant je suis fin prêt à reprendre de l’activité !

## LE BAC

Le lycée, c’est fini pour moi ! J’ai réussi à obtenir la mention Bien avec une moyenne générale de 15.61. Ce dont je ne suis pas peu fier 😀

Dimanche qui vient, je vous proposerai un article sur un nouveau format : Au format vidéo. En effet, après plusieurs demandes je vais vous présenter mon projet d’ISN (Informatique et Sciences du Numérique, spécialité de Terminale S) car j’ai eu 19/20 sur ce projet. Je présenterais peut-être plus tard les autres projets que j’ai fait au cours de l’année qui m’ont permis de majorer la classe toute cette année.

## CoDecember -> KMDC

CoDecember était à l’origine un projet dont j’avais eu l’idée en Décembre 2017 et qui a, avec le bac blanc, les oraux de bac, le bac, … pris du retard. Mais le projet me tenant vraiment à coeur, j’ai continué à travailler dessus lorsque j’avais du temps libre. Plus d’infos arriveront à ce sujet plus tard dans l’été.

Aussi, j’ai décidé de changer le nom du projet pour KMDC. Signifiant **Kelou MediaCenter**, intitulé que je trouvais pertinent pour ce dernier.

Le projet progresse et une version beta est susceptible d’arriver à la fin de l’été ! Mais je tiens avant tout à présenter une application complète et fonctionnelle.

## Formation ‘Créer un CMS de A à Z’

Cette formation a été suivie par beaucoup, et cela me fait très plaisir de voir que mon travail est apprécié et que j’ai pu en aider parmi vous.

Cette formation était avant tout un moyen pour moi de préparer du contenu en avance pour la période de bac et que ceux qui suivent le blog n’aient pas un ‘trou’ d’un mois dans les articles.

Maintenant que c’est l’été, je met en pause cette formation et je peux d’ores et déjà vous dire que la fin de la partie II et la partie III arriveront vers Novembre, période durant laquelle j’aurais mes examens de premier semestre en étude supérieure. (J’apporterai des précisions à cela lorsque j’aurai les dates précises de ces examens).

En parlant de formation, j’ai décidé d’en proposer une pendant les vacances sur le WebDesign et la création de maquete car je trouve important de connaître les étapes menants à la création d’un site étant donné que je vois trop de monde se lancer directement dans du HTML/CSS sans vraiment savoir ce qu’ils vont en faire. 

Un article sortira bientôt pour donner plus de détails sur celle-ci. Elle devrait être composée de 4 chapitres et durera 2 semaines.

## Reprise des TDJ

TDJ signifie ‘Test De Jeu’. Cela concerne les articles avec la balise **[TDJ]**. Pour l’instant je ne l’ai fait que pour 2 jeux (Firewatch et Northguard). Je peux déjà vous dire qu’il y en aura un dédié à **The Crew 2**. Aussi, avec la fin des examens, j’ai pu retrouver du temps pour jouer tout en continuant de m’instruire sur les différents domaines qui me passionnent (nature, vélo, …). 

Je compte donc vous proposer d’autres tests, mais cela concerne plus les prochains mois que les prochaines semaines.

C’est tout pour cette fois, mais je pense que c’est déjà pas mal. L’été comme l’an passé va être riche en contenu et surtout diversifié.

Si vous cela vous plait, je vous invite à vous rendre sur la [page d’accueil du blog](https://kelou.fr/) pour découvrir le contenu déjà proposé. 

Sur ce, je vous dis BONNE VACANCES !
