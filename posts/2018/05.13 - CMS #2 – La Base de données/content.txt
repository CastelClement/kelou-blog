La création de la base de données est un moment crucial du projet car elle définie la structure des données. Bien que modifiable par la suite, il est important d’avoir une base de données (abrégée BDD ou DB (database)) bien ordonnée pour s’y repérer facilement et l’exploiter sans problème.

Pour cette formation, nous allons utiliser un serveur local pour faciliter le développement.

Pour cela, je vous conseille d’installer WAMP (pour Windows, téléchargeable [ICI](http://www.wampserver.com/#download-wrapper)), LAMP (pour Linux, téléchargeable [ICI](https://doc.ubuntu-fr.org/lamp)), XAMPP (pour MacOS, téléchargeable [ICI](https://www.apachefriends.org/fr/index.html)).

L’installation de WAMP n’est pas compliquée, il suffit de suivre ce qui est marqué dans la fenêtre de l’installateur pour s’y retrouver.

Aussi, il existe plusieurs alternatives pour le soft de la BDD (MariaDB, NoSQL, …) mais nous allons utiliser le plus connu: MySQL.

Étant sur Windows, je vais donc utiliser WAMP.

Pour créer notre base de données, nous allons utiliser le logiciel **PhpMyAdmin**. Il est par défaut installé avec WAMP (et je crois que c’est la même chose pour les autres distros.).

Premièrement, il va falloir se connecter à PhpMyAdmin. Pour cela rendez vous à la page “_localhost/phpmyadmin/_”. Un nom d’utilisateur et un mot de passe seront demandés. Par défaut, sur WAMP, le nom d’utilisateur est **‘root’** et il n’y a **pas de mot de passe**, laissez donc ce champ vide et cliquez sur ‘Exécuter’.

![](1.png)

---

![](2-1.png)

Une fois connecté, vous verrez beaucoup d’informations mais cela n’est pas important.

Contentez-vous de cliquez sur ‘**Nouvelle base de données**’ dans le panneau de gauche.

Dans création d’une base de données, nous allons prendre comme nom **‘cms’** et comme système de fichier **‘utf8_general_ci’.** Cliquez ensuite sur **‘Créer’.**

![](3.png)

Maintenant que notre base de données est créée, il faut créer une table. Nous allons la nommer ‘**articles’,** elle contiendra toutes les informations relatives aux articles. Pour savoir le nombre de colonnes à créer, il faut se référer au cahier des charges que nous avons construits la semaine passée. (si vous ne l’avez pas encore lu, l’article est accessible [ICI](https://kelou.fr/cms-1/)).

On sait que chaque article aura: un ID, un Titre, un sous-titre, un Corps de texte, une Date de publication, une Note.

Il faut donc créer 6 colonnes.

![](4.png)

Ensuite, nous devons savoir ce que chaque donnée contiendra.

L’**ID** sera un **entier**, qui s’incrémente automatiquement à chaque article créé. Il faut donc **cocher la case ‘A_I’** (signifiant Auto Increment).

Le **Titre** sera du texte, nous allons nommer cette ligne **‘title’** et lui donner comme type **‘TEXT’.**

Pour le **Sous-titre**, c’est la même chose, sauf que nous allons l’appeler **‘subtitle’.**

Le **Corps de texte** s’appellera **‘content’** (signifiant littéralement ‘contenu’) et sera de type ‘**LONGTEXT**’, car l’on veut pouvoir écrire beaucoup !

La **Date** se nommera **‘date’**, sera de type **‘TIMESTAMP’** et nouveau: nous allons choisir comme valeur par défaut **‘CURRENT_TIMESTAMP’**. C’est à dire, que à chaque fois qu’un article sera publié, cette ligne gardera en mémoire l’heure à laquelle il a été posté.

Pour finir, la dernière case sera **‘note’** et sera de type **‘TEXT’**.

Le **code SQL correspondant** à notre base de donnée est le suivant: 

```
CREATE TABLE cms.articles ( ID INT NOT NULL AUTO_INCREMENT , title TEXT NOT NULL , subtitle TEXT NOT NULL , content LONGTEXT NOT NULL , date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , note TEXT NOT NULL , PRIMARY KEY (ID)) ENGINE = MyISAM;
```

Vous voilà avec une table comme celle-ci:

![](5.png)

Il vous suffit de cliquer sur **‘Enregistrer’** et votre base de données sera **terminée**.

Vous pouvez voir la structure de la table qui s’affiche.

![](6.png)

Cliquez sur **‘articles’** dans le panneau de gauche, et vous verrez l’endroit où vous pourrez par la suite voir les données stockées. Pour l’instant cela est vide, et bien heureusement, car nous n’avons pour l’instant pas travaillé sur cette BDD.

![](7.png)

Voilà pour cette semaine, la semaine prochaine nous allons travailler sur la page d’accueil pour apprendre comment afficher les données qui seront stockées dans le futur.

N’hésitez pas à partager si vous pensez que cela peut aussi permettre d’aider d’autres personnes.

**Merci pour votre présence sur le blog, on avance ensemble !**
