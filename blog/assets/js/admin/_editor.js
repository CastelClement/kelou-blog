/**
 * document ready
 */
var Choices = require('choices.js')
document.addEventListener('DOMContentLoaded', function () {
  const selects = document.getElementsByTagName('select')
  var choices = []
  console.log(selects)
  for (let i = 0; i < selects.length; i++) {
    choices.push(new Choices(selects[i]))
  }
}, false)

/**
 * Autoresize textarea
 */
const tx = document.getElementsByTagName('textarea')
for (let i = 0; i < tx.length; i++) {
  tx[i].setAttribute('style', 'height:' + (tx[i].scrollHeight) + 'px;overflow-y:hidden;')
  tx[i].addEventListener('input', OnInput, false)
}

function OnInput () {
  var scrollLeft = window.pageXOffset || (document.documentElement || document.body.parentNode || document.body).scrollLeft
  var scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop

  this.style.height = 'auto'
  this.style.height = (this.scrollHeight) + 16 + 'px'

  window.scrollTo(scrollLeft, scrollTop)
}

/**
  * Intro character counter
  */
const introTx = document.getElementById('editor-intro')
const introCC = document.getElementById('intro-cc')

if (introTx) {
  introTx.addEventListener('input', () => {
    const len = introTx.value.length
    introCC.innerText = '[' + len + ' / 220]'

    if (len > 150 && len <= 220) { // valid : green
      introCC.setAttribute('style', 'color: green;')
    } else if (len > 220) { // incorrect : red
      introCC.setAttribute('style', 'color: red;')
    } else {
      introCC.setAttribute('style', 'color: #EEE;')
    }
  }, false)
}


/**
  * Editor live render
  */
const contentTx = document.getElementById('editor-content')
const liveCB = document.getElementById('editor-live')
const renderDiv = document.getElementById('render-div')

contentTx.addEventListener('input', () => {
  if (liveCB.checked) {
    var form = new FormData()
    form.append('md', JSON.stringify(contentTx.value))

    fetch('/admin/api/md2html', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ md: contentTx.value })
    })
      .then(res => res.json())
      .then(data => {
        renderDiv.innerHTML = data
      })
  }
}, false)
