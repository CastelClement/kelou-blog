#!/bin/bash
echo "DEPLOY - STAGE 1 : Create archive"
fn="kelou-$(date +'%H%M%S-%d%m%Y')"
tar zcf $(echo $fn).tar.gz assets/ bin/console config/ public/assets/ public/bundles/ public/images/ public/index.php src/ templates/ composer.json composer.lock package.json package-lock.json webpack.config.js
echo "DEPLOY - Archive CREATED ($fn)"
