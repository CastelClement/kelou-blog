<?php

namespace App\Entity;

use App\Repository\MediaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=MediaRepository::class)
 * @Vich\Uploadable()
 */
class Media
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $seo_alt;

    /**
     * @Vich\UploadableField(mapping="media_mediafile", fileNameProperty="mediaName", size="mediaSize")
     * @var File|null
     */
    private $mediaFile;

    /**
     * Media name
     * @ORM\Column(type="string", length=255)
     */
    private $mediaName;

    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $mediaSize;

    /**
     * @ORM\Column(type="datetime")
     */
    private $uploaded_at;

    /**
     * @ORM\OneToMany(targetEntity=Post::class, mappedBy="thumbnail")
     */
    private $posts;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSeoAlt(): ?string
    {
        return $this->seo_alt;
    }

    public function setSeoAlt(string $seo_alt): self
    {
        $this->seo_alt = $seo_alt;

        return $this;
    }

    public function getUploadedAt(): ?\DateTimeInterface
    {
        return $this->uploaded_at;
    }

    public function setUploadedAt(\DateTimeInterface $uploaded_at): self
    {
        $this->uploaded_at = $uploaded_at;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setThumbnail($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getThumbnail() === $this) {
                $post->setThumbnail(null);
            }
        }

        return $this;
    }

    /**
     * @param File|null $mediaFile
     */
    public function setMediaFile(?File $mediaFile)
    {
        $this->mediaFile = $mediaFile;
        if ($mediaFile) {
            $this->uploaded_at = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getMediaFile(): ?File
    {
        return $this->mediaFile;
    }

    /**
     * @param int $mediaSize
     */
    public function setMediaSize(int $mediaSize): Media
    {
        $this->mediaSize = $mediaSize;
        return $this;
    }

    /**
     * @return int
     */
    public function getMediaSize(): ?int
    {
        return $this->mediaSize;
    }

    /**
     * @return string|null
     */
    public function getMediaName() : ?string
    {
        return $this->mediaName;
    }

    /**
     * @param string $mediaName
     */
    public function setMediaName($mediaName): Media
    {
        $this->mediaName = $mediaName;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->getMediaName();
    }
}
