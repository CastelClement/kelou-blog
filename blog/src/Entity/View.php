<?php

namespace App\Entity;

use App\Repository\ViewRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ViewRepository::class)
 */
class View
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $view_date;

    /**
     * @ORM\Column(type="text")
     */
    private $view_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $view_utm_source;

    /**
     * @ORM\Column(type="text")
     */
    private $view_ua;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $view_ip;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getViewDate(): ?\DateTimeInterface
    {
        return $this->view_date;
    }

    public function setViewDate(\DateTimeInterface $view_date): self
    {
        $this->view_date = $view_date;

        return $this;
    }

    public function getViewUrl(): ?string
    {
        return $this->view_url;
    }

    public function setViewUrl(string $view_url): self
    {
        $this->view_url = $view_url;

        return $this;
    }

    public function getViewUtmSource(): ?string
    {
        return $this->view_utm_source;
    }

    public function setViewUtmSource(?string $view_utm_source): self
    {
        $this->view_utm_source = $view_utm_source;

        return $this;
    }


    public function getViewUa(): ?string
    {
        return $this->view_ua;
    }

    public function setViewUa(string $view_ua): self
    {
        $this->view_ua = $view_ua;

        return $this;
    }

    public function getViewIp(): ?string
    {
        return $this->view_ip;
    }

    public function setViewIp(string $view_ip): self
    {
        $this->view_ip = $view_ip;

        return $this;
    }
}
