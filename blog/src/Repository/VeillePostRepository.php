<?php

namespace App\Repository;

use App\Entity\VeillePost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VeillePost|null find($id, $lockMode = null, $lockVersion = null)
 * @method VeillePost|null findOneBy(array $criteria, array $orderBy = null)
 * @method VeillePost[]    findAll()
 * @method VeillePost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VeillePostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VeillePost::class);
    }

    // /**
    //  * @return VeillePost[] Returns an array of VeillePost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VeillePost
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findLive(): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.is_live = true')
            ->orderBy('p.created_at', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findAllOrderIdAdmin()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.created_at', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
