<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Post;
use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * Get all live posts.
     * Two conditions : post live date is before now + post is public
     * @return QueryBuilder
     */
    public function getDefaultLiveQuery () : QueryBuilder
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.is_live = true')
            ->andWhere('p.live_at < :now')
            ->orderBy('p.live_at', 'DESC')
            ->setParameter('now', new \DateTimeImmutable());
    }

    /**
     * @param int $year
     */
    public function findByYear(int $year)
    {
        $start = new \DateTimeImmutable($year . '-01-01');
        $end = new \DateTimeImmutable($year . '-12-31');

        return $this->createQueryBuilder('p')
            ->andWhere('p.is_live = true')
            ->andWhere('p.live_at BETWEEN :start AND :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Query
     */
    public function findAllLiveQuery() : Query
    {
        return $this->getDefaultLiveQuery()
            ->getQuery();
    }

    public function findAllLiveResult()
    {
        return $this->findAllLiveQuery()
            ->getResult();
    }

    /**
     * find scheduled posts
     * only posts ready to be live
     * @return int|mixed|string
     */
    public function findScheduled()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.is_live = true')
            ->andWhere('p.live_at > :now')
            ->orderBy('p.live_at', 'ASC')
            ->setParameter('now', new \DateTimeImmutable())
            ->getQuery()
            ->getResult()
            ;
    }

    public function findLastModified(int $limit)
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.updated_at', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllOrderId()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.is_live = true')
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }
    
    public function findAllOrderIdAdmin()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.is_live = true')
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByTitle($title)
    {
        return $this->getDefaultLiveQuery()
            ->andWhere('p.title LIKE :title')
            ->setParameter('title', "%$title%")
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByTag(Tag $tag)
    {
        return $this->getDefaultLiveQuery()
            ->andWhere(':tag MEMBER OF p.tags')
            ->setParameter('tag', $tag)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByCategory(Category $category)
    {
        return $this->getDefaultLiveQuery()
            ->andWhere(':category = p.category')
            ->setParameter('category', $category)
            ->getQuery()
            ->getResult()
            ;
    }
}
