<?php

namespace App\Repository;

use App\Entity\NewsletterReader;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NewsletterReader|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsletterReader|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsletterReader[]    findAll()
 * @method NewsletterReader[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsletterReaderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewsletterReader::class);
    }

    /**
     * @return int
     */
    public function countInt() : int
    {
        return $this->createQueryBuilder('nlr')
            ->select('count(nlr.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    // /**
    //  * @return NewsletterReader[] Returns an array of NewsletterReader objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NewsletterReader
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
