<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TagFixtures extends Fixture
{

    public const TAG_A_REF = 'tag_a';
    public const TAG_B_REF = 'tag_b';
    public const TAG_C_REF = 'tag_c';

    public function load(ObjectManager $manager)
    {
        $tagA = new Tag();
        $tagA->setLabel('Symfony')
            ->setDescription('descA tag Symfony');
        $manager->persist($tagA);

        $tagB = new Tag();
        $tagB->setLabel('Webpack')
            ->setDescription('descB tag Webpack');
        $manager->persist($tagB);

        $tagC = new Tag();
        $tagC->setLabel('Wordpress')
            ->setDescription('descC tag Wordpress');
        $manager->persist($tagC);

        $tagD = new Tag();
        $tagD->setLabel('Intelligence Artificielle')
            ->setDescription('descD tag Intelligence Artificielle');
        $manager->persist($tagD);

        $tagE = new Tag();
        $tagE->setLabel('DevOps')
            ->setDescription('descE tag DevOps');
        $manager->persist($tagE);

        $tagF = new Tag();
        $tagF->setLabel('CI/CD')
            ->setDescription('descF tag CI/CD');
        $manager->persist($tagF);

        $tagG = new Tag();
        $tagG->setLabel('Python')
            ->setDescription('descG tag Python');
        $manager->persist($tagG);

        $tagH = new Tag();
        $tagH->setLabel('Machine Learning')
            ->setDescription('descH tag Machine Learning');
        $manager->persist($tagH);

        $tagI = new Tag();
        $tagI->setLabel('Marketing')
            ->setDescription('descI tag Marketing');
        $manager->persist($tagI);

        $manager->flush();

        $this->addReference(self::TAG_A_REF, $tagD);
        $this->addReference(self::TAG_B_REF, $tagG);
        $this->addReference(self::TAG_C_REF, $tagH);
    }
}
