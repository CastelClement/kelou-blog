<?php

namespace App\DataFixtures;

use App\Entity\Revision;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class RevisionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $post = $this->getReference(PostFixtures::POST_REF);
        $faker = Factory::create('fr_FR');

        $revisionA = new Revision();
        $revisionA->setTitle($faker->words(8, true))
            ->setIntro($faker->sentences(6, true))
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setCategory($this->getReference(CategoryFixtures::MASTERCLASS_CAT_REF))
            ->setSlug($faker->slug(8))
            ->setLiveAt(new \DateTimeImmutable())
            ->setContent('PREMIÈRE REV')
            ->setPostId($post);
        $manager->persist($revisionA);

        $revisionB = clone $revisionA;
        $revisionB->setTitle('changement titre');
        $manager->persist($revisionB);

        $manager->flush();
    }


    public function getDependencies()
    {
        return array(
            PostFixtures::class,
        );
    }
}
