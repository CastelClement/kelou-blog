<?php

namespace App\DataFixtures;

use App\Entity\LoginAttempt;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LoginAttemptFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // la = login attempt
        $laA = new LoginAttempt();
        $laA->setEmail('monemail@mon-domaine.com')
            ->setSuccessful(false)
            ->setAttemptedAt(new \DateTimeImmutable());
        $manager->persist($laA);

        $laB = new LoginAttempt();
        $laB->setEmail('second-mail@mon-second-domaine.net')
            ->setSuccessful(true)
            ->setAttemptedAt(new \DateTimeImmutable());
        $manager->persist($laB);

        $manager->flush();
    }
}
