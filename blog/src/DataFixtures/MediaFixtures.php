<?php

namespace App\DataFixtures;

use App\Entity\Media;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MediaFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $mediaA = new Media();
        $mediaA->setSeoAlt('premier seo alt')
            ->setMediaName('premier media name')
            ->setMediaSize(45000)
            ->setUploadedAt(new \DateTimeImmutable());
        $manager->persist($mediaA);

        $mediaB = new Media();
        $mediaB->setSeoAlt('second seo alt')
            ->setMediaName('second media name')
            ->setMediaSize(900000)
            ->setUploadedAt(new \DateTimeImmutable());
        $manager->persist($mediaB);

        $mediaC = new Media();
        $mediaC->setSeoAlt('troisième seo alt')
            ->setMediaName('troisième media name')
            ->setMediaSize(1350200)
            ->setUploadedAt(new \DateTimeImmutable());
        $manager->persist($mediaC);

        $manager->flush();
    }
}
