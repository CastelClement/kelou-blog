<?php

namespace App\DataFixtures;

use App\Entity\View;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ViewFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 500; $i++) {
            $view = new View();
            $view->setViewUrl('/post/mon-premier-post')
                ->setViewDate(new \DateTimeImmutable())
                ->setViewUa('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36')
                ->setViewUtmSource('undefined')
                ->setViewIp('127.0.0.1');
            $manager->persist($view);
        }

        $manager->flush();
    }
}
