<?php

namespace App\DataFixtures;

use App\Entity\NewsletterReader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class NewsletterReaderFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // nr = newsletter reader
        $nrA = new NewsletterReader();
        $nrA->setName('Clément')
            ->setEmail('clement@kelou.fr')
            ->setRegisteredAt(new \DateTimeImmutable())
            ->setRegisterUrl('/post/mon-premier-post');
        $manager->persist($nrA);

        $nrB = new NewsletterReader();
        $nrB->setName('Clément BBB')
            ->setEmail('clement-bbb@kelou-bbb.fr')
            ->setRegisteredAt(new \DateTimeImmutable())
            ->setRegisterUrl('/');
        $manager->persist($nrB);


        $manager->flush();
    }
}
