<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{

    public const MASTERCLASS_CAT_REF = 'masterclass_cat_ref';

    public function load(ObjectManager $manager)
    {
        $catA = new Category();
        $catA->setLabel('Développement personnel')
            ->setDescription('une super description !');
        $manager->persist($catA);

        $catB = new Category();
        $catB->setLabel('Informatique')
            ->setDescription('bla bla blaa informatique bla bla');
        $manager->persist($catB);

        $catC = new Category();
        $catC->setLabel('MASTERCLASS')
            ->setDescription('Regroupement des meilleures publications :)');
        $manager->persist($catC);

        $catD = new Category();
        $catD->setLabel('News')
            ->setDescription('Informations à propos du blog');
        $manager->persist($catD);


        $manager->flush();

        $this->addReference(self::MASTERCLASS_CAT_REF, $catC);
    }
}
