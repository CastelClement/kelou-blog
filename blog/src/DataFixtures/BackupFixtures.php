<?php

namespace App\DataFixtures;

use App\Entity\Backup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BackupFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $backupA = new Backup();
        $backupA->setTitle('premier backup')
            ->setDescription('fixture numéro 1 de backup')
            ->setData("CREATE TABLE TestBackup;")
            ->setStartedAt(new \DateTimeImmutable())
            ->setFinishedAt(new \DateTimeImmutable());
        $manager->persist($backupA);

        $backupB = new Backup();
        $backupB->setTitle('SECOND backup')
            ->setDescription('fixture numéro 2 de backup')
            ->setData("CREATE TABLE SecondTestBackup;")
            ->setStartedAt(new \DateTimeImmutable())
            ->setFinishedAt(new \DateTimeImmutable());
        $manager->persist($backupB);

        $manager->flush();
    }
}
