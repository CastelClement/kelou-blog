<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PostFixtures extends Fixture implements DependentFixtureInterface
{

    public const POST_REF = 'post_ref';

    public function load(ObjectManager $manager)
    {
        // Parsedown (MD -> HTML)
        $parsedown = new \Parsedown();
        $parsedown->setBreaksEnabled(true);

        // Fixtures
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 50; $i++) {
            $post = new Post();
            $post->setTitle($faker->words(8, true))
                ->setIntro($faker->sentences(3, true))
                ->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable())
                ->setCategory($this->getReference(CategoryFixtures::MASTERCLASS_CAT_REF))
                ->setSlug($faker->slug(8))
                ->setIsLive(true)
                ->setLiveAt(new \DateTimeImmutable())
                ->setContent('## Post d\'exemple :
## GitHub Markdown test

```
# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:

Alt-H1
======

Alt-H2
------
```

## H2
### H3

Alternatively, for H1 and H2, an underline-ish style:

Alt-H2
------


## Emphasis


```
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~
```

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~


## Lists

```
1. First ordered list item
2. Another item
1. Actual numbers don\'t matter, just that it\'s a number
4. And another item.

⋅⋅⋅You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we\'ll use three here to also align the raw Markdown).

⋅⋅⋅To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
⋅⋅⋅Note that this line is separate, but within the same paragraph.⋅⋅
⋅⋅⋅(This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses
```

1. First ordered list item
2. Another item
⋅⋅* Unordered sub-list.
1. Actual numbers don\'t matter, just that it\'s a number
⋅⋅1. Ordered sub-list
4. And another item.

⋅⋅⋅You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we\'ll use three here to also align the raw Markdown).

⋅⋅⋅To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
⋅⋅⋅Note that this line is separate, but within the same paragraph.⋅⋅
⋅⋅⋅(This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses


## Links

```
[I\'m an inline-style link](https://www.google.com)

[I\'m an inline-style link with title](https://www.google.com "Google\'s Homepage")

[I\'m a reference-style link][Arbitrary case-insensitive reference text]

[I\'m a relative reference to a repository file](../blob/master/LICENSE)

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself]

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com
```

[I\'m an inline-style link](https://www.google.com)

[I\'m an inline-style link with title](https://www.google.com "Google\'s Homepage")

[I\'m a reference-style link][Arbitrary case-insensitive reference text]

[I\'m a relative reference to a repository file](../blob/master/LICENSE)

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself]

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com


## Images

```
Here\'s our logo (hover to see the title text):

Inline-style:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

Reference-style:
![alt text][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"
```

Here\'s our logo (hover to see the title text):

Inline-style:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

Reference-style:
![alt text][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"


## Tables

```
Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned |  |
| col 2 is      | centered      |    |
| zebra stripes | are neat      |     |

The outer pipes (|) are optional, and you don\'t need to make the raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3
```

Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned |  |
| col 2 is      | centered      |    |
| zebra stripes | are neat      |     |

The outer pipes (|) are optional, and you don\'t need to make the raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3


## Blockquotes

```
> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let\'s keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.
```

> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let\'s keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.


## Horizontal Rule

```
Three or more...

---

Hyphens

***

Asterisks

___

Underscores
```

Three or more...

---

Hyphens

***

Asterisks

___

Underscores');
            $post->setContentHtml($parsedown->text($post->getContent()));
            $manager->persist($post);
        }

        $manager->flush();

        $post = new Post();
        $post->setTitle($faker->words(8, true))
            ->setIntro($faker->sentences(4, true))
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setCategory($this->getReference(CategoryFixtures::MASTERCLASS_CAT_REF))
            ->setSlug("mon-premier-post")
            ->setIsLive(true)
            ->setLiveAt(new \DateTimeImmutable())
            ->setContent('MON PREMIER POST')
            ->addTag($this->getReference(TagFixtures::TAG_A_REF))
            ->addTag($this->getReference(TagFixtures::TAG_B_REF))
            ->addTag($this->getReference(TagFixtures::TAG_C_REF));
        $post->setContentHtml($parsedown->text($post->getContent()));
        $manager->persist($post);
        $manager->flush();

        $this->addReference(self::POST_REF, $post);
    }

    public function getDependencies()
    {
        return array(
            TagFixtures::class,
            CategoryFixtures::class,
        );
    }
}
