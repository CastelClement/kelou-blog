<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminAPIController
 * @package App\Controller
 * @Route("/admin/api")
 */
class AdminAPIController extends AbstractController
{

    /**
     * @Route("/md2html", name="admin.api.md2html")
     * @param Request $request
     * @return Response
     */
    public function md2html (Request $request) : Response
    {
        $md = $parameters = json_decode($request->getContent(), true)['md'];

        $parsedown = new \Parsedown();
        $parsedown->setBreaksEnabled(true);

        $html = $parsedown->text($md);

        return new JsonResponse($html);
    }
}
