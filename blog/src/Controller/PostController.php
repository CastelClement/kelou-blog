<?php

namespace App\Controller;

use App\Entity\NewsletterReader;
use App\Entity\NewsletterRegister;
use App\Entity\Post;
use App\Form\NewsletterRegisterFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class PostController extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * PostController constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {

        $this->twig = $twig;
    }

    /**
     * @Route("/post/{slug}", name="post.show")
     * @param Post|null $post
     * @param Request $request
     * @return Response
     */
    public function post (?Post $post, Request $request) : Response
    {
        if ($post) {
            // NEWSLETTER FORM
            $em = $this->getDoctrine()->getManager();
            $newsletterRegister = new NewsletterRegister();
            $nrForm = $this->createForm(NewsletterRegisterFormType::class, $newsletterRegister);
            $nrForm->handleRequest($request);

            if ($nrForm->isSubmitted() && $nrForm->isValid()) {
                $newsletterReader = new NewsletterReader();
                $newsletterReader->setName($newsletterRegister->getName())
                    ->setEmail($newsletterRegister->getEmail())
                    ->setRegisterUrl('/post/' . $post->getSlug())
                    ->setRegisteredAt(new \DateTimeImmutable());

                $em->persist($newsletterReader);
                $em->flush();

                $this->addFlash(
                    'valid',
                    'Votre inscription a bien été prise en compte !<br/> Vous devriez recevoir un email de bienvenue dans les prochaines minutes. Pensez à vérifier vos spams ;)'
                );

                unset($newsletterRegister);
                unset($nrForm);
                $newsletterRegister = new NewsletterRegister();
                $nrForm = $this->createForm(NewsletterRegisterFormType::class, $newsletterRegister);
            }


            return new Response($this->twig->render('home/post.html.twig', [
                'post' => $post,
                'form' => $nrForm->createView(),
            ]));
        } else {
            return $this->redirectToRoute('404');
        }

    }
}
