<?php

namespace App\Controller;

use App\Entity\Media;
use App\Form\MediaType;
use App\Repository\MediaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * Class AdminMediaController
 * @package App\Controller
 * @Route("/admin")
 */
class AdminMediaController extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * AdminMediaController constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/create-media", name="admin.createMedia")
     * @param Request $request
     * @return Response
     */
    public function createMedia (Request $request) : Response
    {
        $em = $this->getDoctrine()->getManager();

        $media = new Media();
        $form = $this->createForm(MediaType::class, $media);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $media->setMediaName($media->getMediaFile()->getFilename());
            $media->setMediaSize($media->getMediaFile()->getSize());

            $em->persist($media);
            $em->flush();
        }


        return new Response($this->twig->render('admin/create-media.html.twig', [
            'media_form' => $form->createView(),
        ]));
    }

    /**
     * @Route("/list-medias", name="admin.listMedia")
     * @param Request $request
     * @return Response
     */
    public function listMedia (Request $request, MediaRepository $mr) : Response
    {
        $mediaRepo = $mr;

        $medias = $mediaRepo->findAllOrdered();

        return new Response($this->twig->render('admin/list-medias.html.twig', [
            'medias' => $medias
        ]));
    }
}
