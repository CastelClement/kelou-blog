<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\NewsletterReader;
use App\Entity\NewsletterRegister;
use App\Form\ContactFormType;
use App\Form\NewsletterRegisterFormType;
use App\Form\SearchType;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use App\Repository\VeillePostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class StaticPageController extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * StaticPageController constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {

        $this->twig = $twig;
    }

    /**
     * @Route("/contact", name="contact")
     * @return Response
     */
    public function contact (Request $request) : Response
    {
        $em = $this->getDoctrine()->getManager();

        $contact = new Contact();
        $form = $this->createForm(ContactFormType::class, $contact);

        if ($msg = $request->query->get('post')) {
            $form->get('message')->setData('À propos de la publication : ' . $msg . '

');
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setSentAt(new \DateTimeImmutable());
            $em->persist($contact);
            $em->flush();

            $this->addFlash(
                'valid',
                'Votre message a bien été envoyé !'
            );

            unset($contact);
            unset($form);
            $contact = new Contact();
            $form = $this->createForm(ContactFormType::class, $contact);
        }



        return new Response($this->twig->render('home/contact.html.twig', [
            'form' => $form->createView(),
        ]));
    }

    /**
     * @Route("/newsletter", name="newsletter")
     * @param Request $request
     * @return Response
     */
    public function newsletter (Request $request) : Response
    {
        // NEWSLETTER FORM
        $em = $this->getDoctrine()->getManager();
        $newsletterRegister = new NewsletterRegister();
        $nrForm = $this->createForm(NewsletterRegisterFormType::class, $newsletterRegister);
        $nrForm->handleRequest($request);

        if ($nrForm->isSubmitted() && $nrForm->isValid()) {
            $newsletterReader = new NewsletterReader();
            $newsletterReader->setName($newsletterRegister->getName())
                ->setEmail($newsletterRegister->getEmail())
                ->setRegisterUrl('/newsletter')
                ->setRegisteredAt(new \DateTimeImmutable());

            $em->persist($newsletterReader);
            $em->flush();

            $this->addFlash(
                'valid',
                'Votre inscription a bien été prise en compte !<br/> Vous devriez recevoir un email de bienvenue dans les prochaines minutes. Pensez à vérifier vos spams ;)'
            );

            unset($newsletterRegister);
            unset($nrForm);
            $newsletterRegister = new NewsletterRegister();
            $nrForm = $this->createForm(NewsletterRegisterFormType::class, $newsletterRegister);
        }



        return new Response($this->twig->render('home/newsletter.html.twig', [
            'form' => $nrForm->createView(),
        ]));
    }


    /**
     * @Route("/search", name="search")
     * @param Request $request
     * @return Response
     */
    public function search (Request $request, PostRepository $postRepository) : Response
    {
        // SEARCH FORM
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        $title = null;
        $posts = null;

        if ($form->isSubmitted() && $form->isValid()) {
            $title = $form->get('title')->getData();
            $posts = $postRepository->findByTitle($title);
        } else {
            $posts = $postRepository->findAllLiveQuery()->getResult();
        }

        return new Response($this->twig->render('home/search.html.twig', [
            'form' => $form->createView(),
            'posts' => $posts,
            'formTitle' => $title,
        ]));
    }

    /**
     * @Route("/archive", name="archive")
     * @param PostRepository $postRepository
     * @return Response
     */
    public function archive (PostRepository $postRepository) : Response
    {
        $year2016 = $postRepository->findByYear(2016);
        $year2017 = $postRepository->findByYear(2017);
        $year2018 = $postRepository->findByYear(2018);
        $year2019 = $postRepository->findByYear(2019);
        $year2020 = $postRepository->findByYear(2020);

        return new Response($this->twig->render('home/archive.html.twig', [
            'year2016' => $year2016,
            'year2017' => $year2017,
            'year2018' => $year2018,
            'year2019' => $year2019,
            'year2020' => $year2020,
        ]));
    }


    /**
     * @Route("/sitemap", name="sitemap")
     * @param PostRepository $postRepository
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @param VeillePostRepository $veillePostRepository
     * @return Response
     */
    public function sitemap (PostRepository $postRepository, CategoryRepository $categoryRepository, TagRepository $tagRepository, VeillePostRepository $veillePostRepository) : Response
    {
        return new Response($this->twig->render('home/sitemap.html.twig', [
            'posts' => $postRepository->findAllLiveResult(),
            'categories' => $categoryRepository->findAll(),
            'tags' => $tagRepository->findAll(),
            'veille_posts' => $veillePostRepository->findLive()
        ]));
    }

    /**
     * @Route("/sitemap.xml", name="sitemap.xml")
     * @param PostRepository $postRepository
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @param VeillePostRepository $veillePostRepository
     * @return Response
     */
    public function sitemapXml (PostRepository $postRepository, CategoryRepository $categoryRepository, TagRepository $tagRepository, VeillePostRepository $veillePostRepository) : Response

    {
        $response =  new Response($this->twig->render('home/sitemap-xml.html.twig', [
            'posts' => $postRepository->findAllLiveResult(),
            'categories' => $categoryRepository->findAll(),
            'tags' => $tagRepository->findAll(),
            'veille_posts' => $veillePostRepository->findLive()
        ]));
        $response->headers->set('Content-Type', 'application/xml');
        return $response;
    }

    /**
     * @Route("/robots.txt", name="robots.txt")
     * @return Response
     */
    public function robotsTxt (Request $request) : Response
    {
        $response =  new Response($this->twig->render('home/robots-txt.html.twig', [
            'domain' => $request->getHost(),
        ]));
        $response->headers->set('Content-Type', 'text/plain');
        return $response;
    }

    /**
     * @Route("/404", name="404")
     * @return Response
     */
    public function error404 () : Response
    {
        return new Response($this->twig->render('home/404.html.twig'));
    }
}
