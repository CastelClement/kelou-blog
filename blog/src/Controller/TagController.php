<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class TagController extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * TagController constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/tag/{label}", name="tag.index")
     * @return Response
     */
    public function tag (?Tag $tag, PostRepository $postRepository) : Response
    {
        if ($tag) {
            return new Response($this->twig->render('home/tag.html.twig', [
                'tag' => $tag,
                'posts' => $postRepository->findByTag($tag)
            ]));
        } else {
            return $this->redirectToRoute('404');
        }
    }
}
