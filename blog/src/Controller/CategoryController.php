<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class CategoryController extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * CategoryController constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/category/{label}", name="category.index")
     * @param Category|null $category
     * @param PostRepository $postRepository
     * @return Response
     */
    public function category (?Category $category, PostRepository $postRepository) : Response
    {
        if ($category) {
            return new Response($this->twig->render('home/category.html.twig', [
                'category' => $category,
                'posts' => $postRepository->findByCategory($category),
            ]));
        } else {
            return $this->redirectToRoute('404');
        }

    }
}
