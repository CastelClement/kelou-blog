<?php

namespace App\Controller;

use App\Entity\NewsletterReader;
use App\Entity\NewsletterRegister;
use App\Entity\Post;
use App\Entity\Revision;
use App\Entity\VeillePost;
use App\Form\MediaType;
use App\Form\NewsletterRegisterFormType;
use App\Form\PostType;
use App\Form\VeillePostType;
use App\Repository\MediaRepository;
use App\Repository\NewsletterReaderRepository;
use App\Repository\PostRepository;
use App\Repository\RevisionRepository;
use App\Repository\VeillePostRepository;
use App\Repository\ViewRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Media;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * Class AdminController
 * @package App\Controller
 * @Route("/admin")
 */
class AdminController extends AbstractController {

    /**
     * @var Environment
     */
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/", name="admin.root")
     * @return Response
     */
    public function root () : Response
    {
        return $this->redirectToRoute('admin.dashboard');
    }

    /**
     * @Route("/dashboard", name="admin.dashboard")
     * @param PostRepository $postRepository
     * @param ViewRepository $viewRepository
     * @return Response
     */
    public function dashboard (PostRepository $postRepository, ViewRepository $viewRepository, NewsletterReaderRepository $newsletterReaderRepository) : Response
    {
        $posts = $postRepository->findAllLiveQuery()->getResult();
        $postsScheduled = $postRepository->findScheduled();
        $postsLastModifed = $postRepository->findLastModified(5);

        $viewsCount = $viewRepository->countInt();
        $nlReadersCount = $newsletterReaderRepository->countInt();

        return new Response($this->twig->render('admin/dashboard.html.twig', [
            'postsCount' => sizeof($posts),
            'viewsCount' => $viewsCount,
            'nlReaders' => $nlReadersCount,
            'postsScheduled' => $postsScheduled,
            'postsLastModified' => $postsLastModifed,
        ]));
    }

    /**
     * @Route("/create-post", name="admin.createPost")
     * @param Request $request
     * @return Response
     */
    public function createPost (Request $request) : Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable());

            // MD -> HTML
            $contentMd = $post->getContent();

            $parsedown = new \Parsedown();
            $parsedown->setBreaksEnabled(true);

            $contentHtml = $parsedown->text($contentMd);
            $post->setContentHtml($contentHtml);

            $em = $this->getDoctrine()->getManager();

            $em->persist($post);
            $em->flush();

            $revision = new Revision();
            $revision->setPostId($post)
                ->setTitle($post->getTitle())
                ->setIntro($post->getIntro())
                ->setCategory($post->getCategory())
                ->setLiveAt($post->getLiveAt())
                ->setSlug($post->getSlug())
                ->setThumbnail($post->getThumbnail())
                ->setContent($post->getContent())
                ->setCreatedAt($post->getCreatedAt())
                ->setUpdatedAt($post->getUpdatedAt());

            $em->persist($revision);
            $em->flush();

            $this->addFlash(
                'valid',
                'Article ajouté !'
            );

            unset($post);
            unset($form);
            $post = new Post();
            $form = $this->createForm(PostType::class, $post);
        }

        return new Response($this->twig->render('admin/createPost.html.twig', [
            'form' => $form->createView(),
            'revisionsCount' => 0,
            'edit' => false,
        ]));
    }

    /**
     * @Route("/edit-post/{id}", name="admin.editPost")
     * @param Request $request
     * @param Post $post
     * @param RevisionRepository $revisionRepository
     * @return Response
     */
    public function editPost (Request $request, Post $post, RevisionRepository $revisionRepository) : Response
    {
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setUpdatedAt(new \DateTimeImmutable());

            // MD -> HTML
            $contentMd = $post->getContent();

            $parsedown = new \Parsedown();
            $parsedown->setBreaksEnabled(true);

            $contentHtml = $parsedown->text($contentMd);
            $post->setContentHtml($contentHtml);

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            $revision = new Revision();
            $revision->setPostId($post)
                ->setTitle($post->getTitle())
                ->setIntro($post->getIntro())
                ->setCategory($post->getCategory())
                ->setLiveAt($post->getLiveAt())
                ->setSlug($post->getSlug())
                ->setThumbnail($post->getThumbnail())
                ->setContent($post->getContent())
                ->setCreatedAt($post->getCreatedAt())
                ->setUpdatedAt($post->getUpdatedAt());

            $em->persist($revision);
            $em->flush();

            $this->addFlash(
                'valid',
                'Article modifié !'
            );
        }

        return new Response($this->twig->render('admin/createPost.html.twig', [
            'form' => $form->createView(),
            'revisionsCount' => sizeof($revisionRepository->findByPostId($post->getId())),
            'edit' => true,
            'title' => $post->getTitle(),
            'post_id' => $post->getId(),
            'createdAt' => $post->getCreatedAt(),
            'updatedAt' => $post->getUpdatedAt(),
            'contentHtml' => $post->getContentHtml(),
        ]));
    }

    /**
     * @Route("/edit-post/revisions/{postId}", name="admin.editPost.revisions")
     * @param RevisionRepository $revisionRepository
     * @return Response
     */
    public function getRevisions (RevisionRepository $revisionRepository, int $postId) : Response
    {
        return new Response($this->twig->render('admin/listRevisions.html.twig', [
            'revisions' => $revisionRepository->findByPostId($postId),
        ]));
    }

    /**
     * @Route("/list-posts", name="admin.listPosts")
     * @param PostRepository $postRepository
     * @return Response
     */
    public function listPosts (PostRepository $postRepository) : Response
    {
        return new Response($this->twig->render('admin/listPosts.html.twig', [
            'posts' => $postRepository->findAllOrderIdAdmin(),
        ]));
    }






    /**
     * @Route("/create-veille-post", name="admin.createVeillePost")
     * @param Request $request
     * @return Response
     */
    public function createVeillePost (Request $request) : Response
    {
        $post = new VeillePost();
        $form = $this->createForm(VeillePostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable());

            // MD -> HTML
            $contentMd = $post->getContent();

            $parsedown = new \Parsedown();
            $parsedown->setBreaksEnabled(true);

            $contentHtml = $parsedown->text($contentMd);
            $post->setContentHtml($contentHtml);

            $em = $this->getDoctrine()->getManager();

            $em->persist($post);
            $em->flush();

            unset($post);
            unset($form);
            $post = new VeillePost();
            $form = $this->createForm(VeillePostType::class, $post);
        }

        return new Response($this->twig->render('admin/createVeillePost.html.twig', [
            'form' => $form->createView(),
            'edit' => false,
        ]));
    }


    /**
     * @Route("/edit-veille-post/{id}", name="admin.editVeillePost")
     * @param Request $request
     * @param VeillePost $post
     * @return Response
     */
    public function editVeillePost (Request $request, VeillePost $post) : Response
    {
        $form = $this->createForm(VeillePostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setUpdatedAt(new \DateTimeImmutable());

            // MD -> HTML
            $contentMd = $post->getContent();

            $parsedown = new \Parsedown();
            $parsedown->setBreaksEnabled(true);

            $contentHtml = $parsedown->text($contentMd);
            $post->setContentHtml($contentHtml);

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
        }

        return new Response($this->twig->render('admin/createVeillePost.html.twig', [
            'form' => $form->createView(),
            'edit' => true,
            'title' => $post->getTitle(),
            'post_id' => $post->getId(),
            'createdAt' => $post->getCreatedAt(),
            'updatedAt' => $post->getUpdatedAt(),
            'contentHtml' => $post->getContentHtml(),
        ]));
    }





    /**
     * @Route("/list-veille-posts", name="admin.listVeillePosts")
     * @param PostRepository $postRepository
     * @return Response
     */
    public function listVeillePosts (VeillePostRepository $veillePostRepository) : Response
    {
        return new Response($this->twig->render('admin/listVeillePosts.html.twig', [
            'posts' => $veillePostRepository->findAllOrderIdAdmin(),
        ]));
    }
}
