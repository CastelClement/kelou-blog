<?php

namespace App\Controller;

use App\Entity\NewsletterReader;
use App\Entity\NewsletterRegister;
use App\Form\NewsletterRegisterFormType;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class HomepageController extends AbstractController {

    /**
     * @var Environment
     */
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }


    /**
     * @Route("/", name="homepage")
     * @param PostRepository $postRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function index (PostRepository $postRepository,
                           PaginatorInterface $paginator,
                           Request $request,
                           CategoryRepository $categoryRepository) : Response
    {
        // PAGINATION
        $page = $request->query->getInt('page', 1);
        $posts = $paginator->paginate(
            $postRepository->findAllLiveQuery(),
            $page,
            9
        );

        // NEWSLETTER FORM
        $em = $this->getDoctrine()->getManager();
        $newsletterRegister = new NewsletterRegister();
        $nrForm = $this->createForm(NewsletterRegisterFormType::class, $newsletterRegister);
        $nrForm->handleRequest($request);

        if ($nrForm->isSubmitted() && $nrForm->isValid()) {
            $newsletterReader = new NewsletterReader();
            $newsletterReader->setName($newsletterRegister->getName())
                ->setEmail($newsletterRegister->getEmail())
                ->setRegisterUrl('/')
                ->setRegisteredAt(new \DateTimeImmutable());

            $em->persist($newsletterReader);
            $em->flush();

            $this->addFlash(
                'valid',
                'Votre inscription a bien été prise en compte !<br/> Vous devriez recevoir un email de bienvenue dans les prochaines minutes. Pensez à vérifier vos spams ;)'
            );

            unset($newsletterRegister);
            unset($nrForm);
            $newsletterRegister = new NewsletterRegister();
            $nrForm = $this->createForm(NewsletterRegisterFormType::class, $newsletterRegister);
        }


        return new Response($this->twig->render('home/homepage.html.twig', [
            'posts' => $posts,
            'categories' => $categoryRepository->findAll(),
            'page' => $page,
            'newsletterForm' => $nrForm->createView(),
        ]));
    }
}