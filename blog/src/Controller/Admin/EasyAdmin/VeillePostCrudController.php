<?php

namespace App\Controller\Admin\EasyAdmin;

use App\Entity\VeillePost;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class VeillePostCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return VeillePost::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
