<?php

namespace App\Controller\Admin\EasyAdmin;

use App\Entity\View;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ViewCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return View::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
