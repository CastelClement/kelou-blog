<?php

namespace App\Controller\Admin\EasyAdmin;

use App\Entity\Backup;
use App\Entity\Category;
use App\Entity\Contact;
use App\Entity\LoginAttempt;
use App\Entity\Media;
use App\Entity\NewsletterReader;
use App\Entity\Post;
use App\Entity\Revision;
use App\Entity\Tag;
use App\Entity\VeillePost;
use App\Entity\View;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin/easy_admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('KELOU Easy Admin');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Backup', 'fa fa-database', Backup::class);
        yield MenuItem::linkToCrud('Catégorie', 'fa fa-list-ul', Category::class);
        yield MenuItem::linkToCrud('LoginAttempt', 'fa fa-sign-in-alt', LoginAttempt::class);
        yield MenuItem::linkToCrud('Media', 'fa fa-photo-video', Media::class);
        yield MenuItem::linkToCrud('NewsletterReader', 'fa fa-book-reader', NewsletterReader::class);
        yield MenuItem::linkToCrud('Post', 'fa fa-newspaper', Post::class);
        yield MenuItem::linkToCrud('Revision', 'fa fa-eraser', Revision::class);
        yield MenuItem::linkToCrud('Tag', 'fa fa-tags', Tag::class);
        yield MenuItem::linkToCrud('View', 'fa fa-eye', View::class);
        yield MenuItem::linkToCrud('Contact', 'fa fa-envelope', Contact::class);
        yield MenuItem::linkToCrud('VeillePost', 'fa fa-lightbulb', VeillePost::class);
    }
}
