<?php

namespace App\Controller\Admin\EasyAdmin;

use App\Entity\Backup;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class BackupCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Backup::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
