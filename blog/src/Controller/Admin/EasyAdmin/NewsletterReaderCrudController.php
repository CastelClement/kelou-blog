<?php

namespace App\Controller\Admin\EasyAdmin;

use App\Entity\NewsletterReader;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class NewsletterReaderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return NewsletterReader::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
