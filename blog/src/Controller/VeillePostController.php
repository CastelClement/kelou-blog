<?php

namespace App\Controller;

use App\Entity\VeillePost;
use App\Repository\VeillePostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class VeillePostController extends AbstractController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * VeillePostController constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/veille/posts", name="veille.index")
     * @param VeillePostRepository $veillePostRepository
     * @return Response
     */
    public function index(VeillePostRepository $veillePostRepository)
    {
        return new Response($this->twig->render('home/veille-index.html.twig', [
            'veille_posts' => $veillePostRepository->findLive(),
        ]));
    }

    /**
     * @Route("/veille/post/{slug}", name="veille.show")
     * @param VeillePost|null $veillePost
     * @return Response
     */
    public function veille_post (?VeillePost $veillePost) : Response
    {
        if ($veillePost) {
            return new Response($this->twig->render('home/veille-post.html.twig', [
                'post' => $veillePost
            ]));
        } else {
            return $this->redirectToRoute('404');
        }
    }
}
