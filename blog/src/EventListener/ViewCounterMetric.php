<?php

namespace App\EventListener;

use App\Entity\View;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class ViewCounterMetric {
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ViewCounterMetric constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    // View counter
    public function onKernelRequest (RequestEvent $event) {
        $request = $event->getRequest();
        if (!$event->isMasterRequest() || str_contains($request->getPathInfo(), '_wdt') || str_contains($request->getPathInfo(), '/admin')) { // not a http request OR admin request
            return;
        }



        $view = new View();
        $view->setViewDate(new \DateTimeImmutable())
            ->setViewUrl($request->getPathInfo())
            ->setViewUa($request->headers->get('user-agent'))
            ->setViewIp(IpUtils::anonymize($request->getClientIp()))
            ->setViewUtmSource("undefined");

        $this->em->persist($view);
        $this->em->flush();
    }
}