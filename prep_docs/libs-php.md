## Librairies PHP utilisées

* cocur/slugify : Générer un slug à partir d'une chaine de caractères
* erusev/parsedown : MD -> HTML
* knplabs/knp-paginator-bundle : Pagination (pour la page d'accueil)
* symfony/webpack-encore-bundle : Intégration de Webpack
* vich/uploader-bundle : Gestion des fichiers

### dev-only:

* fzaninotto/faker : Génération de fixtures pour les tests