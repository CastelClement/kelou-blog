## Exemple : 
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|nom1 |type1 |yes/no |description1 |

---

# kelou-blog

### User
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|username |TextType |- |- |
|email |EmailType |- |- |
|password |PasswordType |- |- |
|avatar |ManyToOne |yes |**@ Media[id]** image de 512x512px |
|created_at |DateTimeType |- |- |

### Login_attempt
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|email |EmailType |- |- |
|attempted_at |DateTimeType |- |- |
|successful |IntegerType |- |Est-ce que la tentative est un succès ? (vrai/faux) |

### Post
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|title |TextType |- |- |
|slug |TextType |- |- |
|intro |TextareaType |- |- |
|content |TextareaType |- |- |
|thumbnail |ManyToOne |yes |**@ Media[id]** |
|is_live |IntegerType |- |Non archivé (public) ? defaut oui |
|live_at |DateTimeType |- |Date de publication |
|created_at |DateTimeType |- |- |
|updated_at |DateTimeType |- |- |
|type |ChoiceType |- |(Post/Page) |
|state |ChoiceType |- |État de l'article (idée -> écriture -> relecture -> publication) |
|revisions |IntegerType |- |Nombre de révisions |
|categories |ManyToMany |yes |**@ Category[id]** |
|tags |ManyToMany |yes |**@ Tag[id]** |


### Revision (une ligne = une save d'un Post)
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|post_id |IntegerType |- |ID du Post correspondant |
|title |TextType |yes |- |
|slug |TextType |yes |- |
|intro |TextareaType |yes |- |
|content |TextareaType |yes |- |
|thumbnail |TextType |yes |ID du média |
|live_at |DateTimeType |yes |Date de publication |
|created_at |DateTimeType |yes |- |
|updated_at |DateTimeType |yes |- |
|type |TextType |yes |(Post/Page) |
|state |TextType |yes |État de l'article (idée -> écriture -> relecture -> publication) |
|revisions |IntegerType |yes |Nombre de révisions |
|categories |TextType |yes |Toutes les catégories séparées par des "," |
|tags |ManyToMany |yes |Tous les tags séparées par des "," |

### Draft (brouillon)
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|title |TextType |- |- |
|content |TextareaType |- |- |
|created_at |DateTimeType |- |- |
|updated_at |DateTimeType |- |- |


### Backup
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|title |TextType |- |- |
|description |TextType |yes |- |
|data |FileType |- |Contenu du dump (fichier) |
|started_at |DateTimeType |- |- |
|finished_at |DateTimeType |- |- |

### Tag
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|label |TextType |- |- |
|description |TextType |- |- |

### Category
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|label |TextType |- |- |
|description |TextType |- |- |

### Media
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|seo_alt |TextType |- |- |
|media |FileType |- |- |
|uploaded_at |DateTimeType |- |- |
|type |ChoiceType |- |(image, video) |

### View
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|view_date|DateTimeType|- |- |
|view_url|TextType |- |- |
|view_utm_source|TextType |- |- |
|view_os|TextType|-|-|
|view_browser|TextType|-|-|
|view_res|TextType|-|Résolution de l'écran |
|view_ua|TextType|-|User Agent |

---

### Newsletter_reader
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|name |TextType |- |- |
|email |EmailType |- |- |
|registered_at |DateTimeType |- |- |
|register_post |ManyToOne |- |**@ Post[id]** : Post sur lequel la personne s'est inscrite à la NL |

### Newsletter_email
|Nom|Type|Nullable?|Description|
|-|-|-|-|
|intro |TextareaType |- |- |
|body |TextareaType |- |- |
|updated_at |DateTimeType |- |- |
|send_at |DateTimeType |- |Date d'envoie à la liste d'emails |
